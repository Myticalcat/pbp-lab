from django.urls import path
from .views import index
from .views import add_notes
from .views import note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_notes, name='add-note'),
    path('note-list', note_list, name='note_list'),
]
