from django import forms
from django.forms import widgets
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ["title", 'From', "to", "message"]
        message = forms.Textarea()


        widgets = {
            'title' : forms.TextInput(attrs={'class'  : 'p-4 m-2 bg-green-100 text-green-700'}),
            'From' : forms.TextInput(attrs={'class'   : 'p-4 m-2 bg-green-100 text-green-700'}),
            'to' : forms.TextInput(attrs={'class'     : 'p-4 m-2 bg-green-100 text-green-700'}),
            'message' : forms.Textarea(attrs={'class' : 'p-4 m-2 bg-green-100 text-green-700'}),
        }

