##### 1. Apakah perbedaan antara JSON dan XML?
JSON adalah data-interchange format yang berdasarkan JavaScript object notation, namun bisa digunakan pada bahasa lain (Independent). XML didesain untuk mengantarakan data, XML adalah kepanjangan dari Extensible Markup Language, sehingga dari visual lebih mirip ke HTML.
Selain itu perbedaannya adalah
| JSON| XML |
| ----------- | ----------- |
| berdasarkan dari JavaScript | turunan dari SGML |
| tidak support namepaces | support namepaces |
| support array   | tidak support array |
| lebih mudah dibaca dengan sekilas | lumayan sulit dibaca |
| hanya support UTF-8   | support beberapa encoding |
| tidak ada comment   | ada comment |

##### 2. Apakah perbedaan antara HTML dan XML?

HTML didesain untuk menampilkan web pages static. Sementara XML didesain untuk mentransfer data. Keduanya adalah markup langauge, namun pada HTML tagnya sudah sudah ditentukan sebelumnya, sementara itu pada XML tagnya dapat kita tentukan.
Sleain itu perbedaanya adalah
| HTML | XML |
| ----------- | ----------- |
| Untuk menampilkan data | Untuk membawa data |
| Static | Dynamic |
| Bisa ada beberapa error | Tidak boleh ada error |
| Case sensitive   | Tidak case sensitive |
| lebih mudah dibaca dengan sekilas | lumayan sulit dibaca |
| Tagnya untuk menampilkan data  | Tagnya untuk menyimpan data |
| Tidak perlu closing tag | Perlu closing tag |


##### Sumber
* [json vs xml](https://www.geeksforgeeks.org/difference-between-json-and-xml/)
* [html vs xml](https://www.geeksforgeeks.org/html-vs-xml/)