console.log('masuk')
$(document).ready( () =>{
    alert("aye aye")
    let listPertanyaan = [
        "Apakah indera penciuman atau perasa anda pernah hilang?", 
        "Apakah anda pernah kontak dengan orang yang terkonfirmasi kasus covid?",
        "Apakah anda pernah mengalami gejala dibawah ini? (Bisa lebih dari satu)",
        "Apakah saturasi oksigen anda kurang dari 92%?",
        "Apakah anda pernah mengalami sesak napas dalam beberapa hari terakhir?",
        "Apakah pernyataan dibawah ini ada yang menggambarkan diri anda?",
        "Pilih Provinsi anda"
    ];
    
    let radio = [1, 2, 4, 5];
    
    let scoreAssessment = 0;
    let pertanyaanCounter = 0;
    let lokasi = "";
    let nama = "";
    
    $("#next-q").click(function(){
        if(scoreAssessment === 0){
            $("#a0-con").fadeOut();
            nama = $('#nama').val();
        }
    
        pertanyaanCounter++;
        console.log(pertanyaanCounter)
        if(pertanyaanCounter === 8){
            cekSemua();
            return
        }
    
        if(radio.includes(pertanyaanCounter-1)){
            if(!$(`input[name='a${pertanyaanCounter-1}']:checked`).val()){
                alert("Pertanyaan ini wajib dijawab");
                pertanyaanCounter--;
                return;
            }
        }
    
        $("#question-navigator").fadeOut(100, () =>{
            if(pertanyaanCounter > 1) $("#back-q").show();
            // hide yg sebelumnya
            if(pertanyaanCounter === 1){
                $("#question").fadeOut(300, () => {
                    $("#question").text(listPertanyaan[0]);
                    $("#question").fadeIn(300);
                    $("#a" + pertanyaanCounter + "-con").fadeIn(300);
                    $("#question-navigator").fadeIn(300);
                });
                return;
            }
            $("#question").fadeOut(300);
            $("#a" + (pertanyaanCounter - 1) + "-con").fadeOut(300, () => {
                $("#question").text(listPertanyaan[pertanyaanCounter-1]);
                $("#question").fadeIn(300);
                $("#a" + pertanyaanCounter + "-con").fadeIn(300);
                $("#question-navigator").fadeIn(300);   
            });
            
        })
    })
    
    $("#back-q").click(function(){
        pertanyaanCounter--;
        
    
        $("#question-navigator").fadeOut(100, () =>{
            if(pertanyaanCounter <= 1) $("#back-q").hide();
            // hide yg setelahnya
            $("#question").fadeOut(300);
            $("#a" + (pertanyaanCounter + 1) + "-con").fadeOut(300, () => {
                $("#question").text(listPertanyaan[pertanyaanCounter-1]);
                $("#question").fadeIn(300);
                $("#a" + pertanyaanCounter + "-con").fadeIn(300);
                $("#question-navigator").fadeIn(300);
            })
        })
    });
    
    function cekSemua(){
        pertanyaanCounter--;
        lokasi = $('select[name=selector] option').filter(':selected').val();
        if ($(":checkbox[name='a6']").is(":checked")){
            // kena covid
            // location.replace("");
            return;
        }
        console.log($('.jawab:checked'))
        $('.jawab:checked').each((index, element) => {
            scoreAssessment += parseInt($(element).val());
            console.log(scoreAssessment)
        })
    
        if(scoreAssessment >= 4){
            // kena covid
            // location.replace("");
            return;
        }
        //ga kena covid
    }
})